(*
Question 1 

In the tutorial, we have given two functions curry and uncurry
that converts between these two froms for bi-variate
functions. Write the tri-variate (i.e. 3 variable) versions of
curry and uncurry functions.  First step is to write down the
type (in the comments). 
*)

(* curry   : ('a * 'b * 'c -> 'd) -> ('a -> 'b -> 'c -> 'd) *)
fun curry f  x y z = f (x,y,z)

(* uncurry : ('a -> 'b -> 'c -> d) -> ('a * 'b * 'c -> 'd) *)
fun uncurry  f  (x,y,z) = f  x y z



(*
Question 2

Write the functions fst : 'a * 'b -> 'a and snd : 'a * 'b -> 'b
that project a tuple into its components.
*)

(* function fst: 'a*'b -> 'a *)
fun fst (x,y) = x

(* function snd : 'a*'b -> 'b *)
fun snd (x,y) = y



(*
Question 3 

Write the length function for lists length : 'a list -> int.
*)
fun length [] = 0 (* for empty list *)
    |length (x::xs) = 1 + length xs (* inductively defining length of a list *)



    
(*
Question 4 

Write the reverse : 'a list -> 'a list function. Be careful to
not make it O(n^2)
*)
(*
[1,2,3][4,5,6]
[3,2,1][4,5,6]
[4,3,2,1][5,6]
[5,4,3,2,1][6]
[6,5,4,3,2,1]
*) 
fun reverse xs =
let
  fun revhelp [] ys = ys
    | revhelp (x::xs) ys = revhelp xs (x::ys)
in
  revhelp xs []
end;



(*
Question 5

Write a function to compute the nth element in the Fibonacci
sequence fib : int -> int. Be careful for the obvious version is
exponential.
*)  
fun fib 0 = 1
| fib 1 = 1
| fib (n) = fib (n-1) + fib (n-2)


(* THIS THE END OF LAB 1 *)      
