

(*
 Question 1
 
 For lists define the function map : ('a -> 'b) -> 'a list -> 'b list. The semantics of map is that it applies the given function
on all the elements of the list, i.e.

 map f [x1, x2,....,xn] = [f x1, f x2, ... f xn] 
 *)
 
 (*
 fun f: 'a -> 'b
 map : ('a -> 'b) -> 'a list -> 'b list
 *)
 fun f x = x
 fun map f [] = []
     |map f (x::xs) = f(x)::map f(xs)
     
     
     
     

(* 
Question 2 

Define the data type 'a tree that captures a binary tree.
*)

(* data type 'a tree = nulltree
     |node of 'a tree*'a*'a tree
*)
datatype 'a tree = NULL
     |Node of 'a tree*'a * 'a tree
     
     
     
     
     
(*
Question 3 

a function `treemap` analogues to `map` for list

treemap: ('a->'b) -> 'atree -> 'btree
*)
fun treemap f NULL = NULL
	|treemap f (Node(tl,data,tr)) = Node(treemap  f tl, f data,treemap f tr)
	
	
	
	
	

(*
Question 4 

Define the in-order, pre-order and post-order traversal of the
binary tree returning the list of nodes in the given order.

fun traversal : 'a tree -> 'a list
*)

fun inorder NULL = []
	| inorder (Node(tl,x,tr)) = inorder tl @ x:: inorder tr
	
fun preorder NULL = []
	| preorder (Node(tl,x,tr)) = (x:: preorder tl) @ preorder tr
	
fun postorder NULL = []
	| postorder (Node(tl,x,tr)) = postorder tl @ postorder tr @ [x]
	
	
	

(*
Question 5 

5. Define the rotate clockwise function for binary trees. Pictorially
this rotation function is defined as the following.

            a                   b
           / \                 / \
          /  \                /   \
         b   🌲₃  ======>   🌲₁   a
        / \                       / \
       /  \                     /   \
      🌲₁ 🌲₂                 🌲₂  🌲₃


If the left subtree of the root is null then rotation is identity operation.
*)
fun rotate (Node(NULL,data,tree)) = Node(NULL,data,tree)
	|rotate (Node(Node(t1,data,t2),a,t3))	  = Node(t1,data,Node(t2,a,t3))
	
	     
